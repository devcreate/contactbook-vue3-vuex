import { createStore } from "vuex";

export default createStore({
  state: {
    contacts: [],
    loading: true,
    isPopupOpen: false,
    selectedContact: null,
  },
  getters: {
    allContacts(state) {
      return state.contacts;
    },
  },
  mutations: {
    updateContacts(state, contacts) {
      state.contacts = contacts;
    },
    updateLoader(state) {
      state.loading = false;
    },
    createContact(state, newContact) {
      state.contacts.unshift(newContact);
    },
    deleteContact(state, id) {
      state.contacts = state.contacts.filter((t) => t.id !== id);
    },
    openPopup(state) {
      state.isPopupOpen = true;
    },
    closePopup(state) {
      state.isPopupOpen = false;
    },
    chosenContact(state, item) {
      state.selectedContact = JSON.parse(JSON.stringify(item));
    },
    changeContact(state) {
      let original = state.contacts.filter(
        (t) => t.id == state.selectedContact.id
      );
      if (
        original.name !== state.selectedContact.name ||
        original.phone !== state.selectedContact.phone ||
        original.email !== state.selectedContact.email
      ) {
        original[0].name = state.selectedContact.name;
        original[0].phone = state.selectedContact.phone;
        original[0].email = state.selectedContact.email;
      }
    },
  },
  actions: {
    async fetchContacts({ commit }, limit = 3) {
      const res = await fetch(
        "https://jsonplaceholder.typicode.com/users?_limit=" + limit
      );
      const contacts = await res.json();
      commit("updateContacts", contacts);
      commit("updateLoader");
    },
    updateChosenContact({ commit }, contact) {
      commit("chosenContact", contact);
    },
  },
});
